import cv2
from keras.models import load_model
import numpy as np
import matplotlib.pyplot as plt
import random
import tensorflow as tf
import os

seed = 2019
random.seed = seed
np.random.seed = seed
tf.seed = seed

pretrained_model = False
model_path = 'vgg16_weights.npz'
image_size = 320
train_batch_size = 10
max_epochs = 10
log_dir = 'tensor_log'

label = ["Apple", "Banana", "Blueberry", "Cherry", "Chestnut", "Kaki", "Kiwi", "Lemon", "Limes", "Lychee", "Mango", "Orange", "Papaya", "Pear", "Pepino", "Physalis", "Pineapple", "Plum", "Pomegranate", "Raspberry", "Salak", "Strawberry", "Tomato"]

def processing(lst_path, size, check= False):

    im = cv2.imread(lst_path, cv2.IMREAD_UNCHANGED)
    im = cv2.resize(im, size)

    if check:
        im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
        # im = np.expand_dims(im,axis=2)
    im = im / 255.

    return np.array(im)

# image = cv2.imread('lychee.jpg', cv2.IMREAD_GRAYSCALE)
# image = cv2.resize(image, (224, 224))
# image = X = np.array(image).reshape(-1, 224, 224, 1)
# model = load_model('Fruit_Regconition.h5')
# predicted = model.predict(image).argmax(-1)
# print(predicted)

input = processing('image/Unet/Image/1.jpg', (320, 320))

# sess = tf.Session()
# init = tf.global_variables_initializer()
# sess.run(init)
# saver = tf.train.Saver()
# saver.restore(sess, 'model/model.ckpt')

print(input.shape)




