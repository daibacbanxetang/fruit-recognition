import cv2
import numpy as np

dim = (320, 320)

path_mask = 'image/Unet/Mask/Image/1.png'
path_image = 'image/Unet/Image/1.jpg'

image = cv2.imread(path_image, cv2.IMREAD_UNCHANGED)
mask = cv2.imread(path_mask, cv2.IMREAD_UNCHANGED)
image = cv2.resize(image, dim)
mask = cv2.resize(mask, dim)

imgray2 = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY)
ret,thresh = cv2.threshold(imgray2,127,255,0)
contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

cv2.drawContours(image, contours, -1, (0,255,0), 1)

cv2.imshow('image', image)
cv2.imshow('mask', mask)


cv2.waitKey(0)
cv2.destroyWindow()