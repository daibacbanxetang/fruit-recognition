import os
import cv2
import random
import tensorflow as tf
import numpy as np

seed = 2019
random.seed = seed
np.random.seed = seed
tf.seed = seed

# from utility import data_batch
pretrained_model = False
model_path = 'vgg16_weights.npz'
image_size = 320
train_batch_size = 10
max_epochs = 10
log_dir = 'tensor_log'

image_input = tf.placeholder(tf.float32, shape=(None, image_size, image_size, 3), name='input_tensor')
mask = tf.placeholder(tf.float32, shape=(None, image_size, image_size))
# mask_all = tf.concat(mask_input_ao, axis=3)
en_parameters = []

def processings(lst_path, size, check= False):

    im = cv2.imread(lst_path, cv2.IMREAD_UNCHANGED)
    im = cv2.resize(im, size)

    if check:
        im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
        # im = np.expand_dims(im,axis=2)
    im = im / 255.

    return np.array(im)


def processing(lst_path, size, check= False):
    lst = []
    for i in lst_path:
        try:
            im = cv2.imread(i)
            im = cv2.resize(im, size)

            if check:
                im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
                # im = np.expand_dims(im,axis=2)
            im = im / 255.
            lst.append(im)
        except:
            pass
    return np.array(lst)




# current_batch_size = image_batch.get_shape().as_list()[0]
# conv1_1
with tf.name_scope('conv1_1') as scope:
    kernel = tf.Variable(tf.truncated_normal([3, 3, 3, 64], dtype=tf.float32,
                                             stddev=1e-1), name='weights')
    tf.summary.histogram('W1_1', kernel)
    conv = tf.nn.conv2d(image_input, kernel, [1, 1, 1, 1], padding='SAME')
    biases = tf.Variable(tf.constant(0.0, shape=[64], dtype=tf.float32),
                         trainable=True, name='biases')
    out = tf.nn.bias_add(conv, biases)
    conv1_1 = tf.nn.relu(out, name=scope)
    en_parameters += [kernel, biases]

# conv1_2
with tf.name_scope('conv1_2') as scope:
    kernel = tf.Variable(tf.truncated_normal([3, 3, 64, 64], dtype=tf.float32, stddev=1e-1), name='weights')
    tf.summary.histogram('W1_2', kernel)
    conv = tf.nn.conv2d(conv1_1, kernel, [1, 1, 1, 1], padding='SAME')
    biases = tf.Variable(tf.constant(0.0, shape=[64], dtype=tf.float32),  trainable=True, name='biases')
    out = tf.nn.bias_add(conv, biases)
    conv1_2 = tf.nn.relu(out, name=scope)
    en_parameters += [kernel, biases]    #320

# pool1
pool1 = tf.nn.max_pool(conv1_2, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME', name='pool1') # 160

# conv2_1
with tf.name_scope('conv2_1') as scope:
    kernel = tf.Variable(tf.truncated_normal([3, 3, 64, 128], dtype=tf.float32, stddev=1e-1), name='weights')
    tf.summary.histogram('W2_1', kernel)
    conv = tf.nn.conv2d(pool1, kernel, [1, 1, 1, 1], padding='SAME')
    biases = tf.Variable(tf.constant(0.0, shape=[128], dtype=tf.float32), trainable=True, name='biases')
    out = tf.nn.bias_add(conv, biases)
    conv2_1 = tf.nn.relu(out, name=scope)
    en_parameters += [kernel, biases]

# conv2_2
with tf.name_scope('conv2_2') as scope:
    kernel = tf.Variable(tf.truncated_normal([3, 3, 128, 128], dtype=tf.float32, stddev=1e-1), name='weights')
    tf.summary.histogram('W2_2', kernel)
    conv = tf.nn.conv2d(conv2_1, kernel, [1, 1, 1, 1], padding='SAME')
    biases = tf.Variable(tf.constant(0.0, shape=[128], dtype=tf.float32), trainable=True, name='biases')
    out = tf.nn.bias_add(conv, biases)
    conv2_2 = tf.nn.relu(out, name=scope)
    en_parameters += [kernel, biases]     #160

# pool2
pool2 = tf.nn.max_pool(conv2_2, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME', name='pool2') #80

# conv3_1
with tf.name_scope('conv3_1') as scope:
    kernel = tf.Variable(tf.truncated_normal([3, 3, 128, 256], dtype=tf.float32, stddev=1e-1), name='weights')
    conv = tf.nn.conv2d(pool2, kernel, [1, 1, 1, 1], padding='SAME')
    biases = tf.Variable(tf.constant(0.0, shape=[256], dtype=tf.float32), trainable=True, name='biases')
    out = tf.nn.bias_add(conv, biases)
    conv3_1 = tf.nn.relu(out, name=scope)
    en_parameters += [kernel, biases]

# conv3_2
with tf.name_scope('conv3_2') as scope:
    kernel = tf.Variable(tf.truncated_normal([3, 3, 256, 256], dtype=tf.float32,
                                             stddev=1e-1), name='weights')
    conv = tf.nn.conv2d(conv3_1, kernel, [1, 1, 1, 1], padding='SAME')
    biases = tf.Variable(tf.constant(0.0, shape=[256], dtype=tf.float32),
                         trainable=True, name='biases')
    out = tf.nn.bias_add(conv, biases)
    conv3_2 = tf.nn.relu(out, name=scope)
    en_parameters += [kernel, biases]   #80

# conv3_3
with tf.name_scope('conv3_3') as scope:
    kernel = tf.Variable(tf.truncated_normal([3, 3, 256, 256], dtype=tf.float32,
                                             stddev=1e-1), name='weights')
    conv = tf.nn.conv2d(conv3_2, kernel, [1, 1, 1, 1], padding='SAME')
    biases = tf.Variable(tf.constant(0.0, shape=[256], dtype=tf.float32),
                         trainable=True, name='biases')
    out = tf.nn.bias_add(conv, biases)
    conv3_3 = tf.nn.relu(out, name=scope)
    en_parameters += [kernel, biases]   #80

# pool3
pool3 = tf.nn.max_pool(conv3_3, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME', name='pool3')  #40

# conv4_1
with tf.name_scope('conv4_1') as scope:
    kernel = tf.Variable(tf.truncated_normal([3, 3, 256, 512], dtype=tf.float32,
                                             stddev=1e-1), name='weights')
    conv = tf.nn.conv2d(pool3, kernel, [1, 1, 1, 1], padding='SAME')
    biases = tf.Variable(tf.constant(0.0, shape=[512], dtype=tf.float32),
                         trainable=True, name='biases')
    out = tf.nn.bias_add(conv, biases)
    conv4_1 = tf.nn.relu(out, name=scope)
    en_parameters += [kernel, biases]

# conv4_2
with tf.name_scope('conv4_2') as scope:
    kernel = tf.Variable(tf.truncated_normal([3, 3, 512, 512], dtype=tf.float32,
                                             stddev=1e-1), name='weights')
    conv = tf.nn.conv2d(conv4_1, kernel, [1, 1, 1, 1], padding='SAME')
    biases = tf.Variable(tf.constant(0.0, shape=[512], dtype=tf.float32),
                         trainable=True, name='biases')
    out = tf.nn.bias_add(conv, biases)
    conv4_2 = tf.nn.relu(out, name=scope)
    en_parameters += [kernel, biases]

# conv4_3
with tf.name_scope('conv4_3') as scope:
    kernel = tf.Variable(tf.truncated_normal([3, 3, 512, 512], dtype=tf.float32,
                                             stddev=1e-1), name='weights')
    conv = tf.nn.conv2d(conv4_2, kernel, [1, 1, 1, 1], padding='SAME')
    biases = tf.Variable(tf.constant(0.0, shape=[512], dtype=tf.float32),
                         trainable=True, name='biases')
    out = tf.nn.bias_add(conv, biases)
    conv4_3 = tf.nn.relu(out, name=scope)
    en_parameters += [kernel, biases]   #40

# pool4
pool4 = tf.nn.max_pool(conv4_3, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME', name='pool4')  #20

# conv5_1
with tf.name_scope('conv5_1') as scope:
    kernel = tf.Variable(tf.truncated_normal([3, 3, 512, 512], dtype=tf.float32,
                                             stddev=1e-1), name='weights')
    conv = tf.nn.conv2d(pool4, kernel, [1, 1, 1, 1], padding='SAME')
    biases = tf.Variable(tf.constant(0.0, shape=[512], dtype=tf.float32),
                         trainable=True, name='biases')
    out = tf.nn.bias_add(conv, biases)
    conv5_1 = tf.nn.relu(out, name=scope)
    en_parameters += [kernel, biases]

# conv5_2
with tf.name_scope('conv5_2') as scope:
    kernel = tf.Variable(tf.truncated_normal([3, 3, 512, 512], dtype=tf.float32,
                                             stddev=1e-1), name='weights')
    conv = tf.nn.conv2d(conv5_1, kernel, [1, 1, 1, 1], padding='SAME')
    biases = tf.Variable(tf.constant(0.0, shape=[512], dtype=tf.float32),
                         trainable=True, name='biases')
    out = tf.nn.bias_add(conv, biases)
    conv5_2 = tf.nn.relu(out, name=scope)
    en_parameters += [kernel, biases]

# conv5_3
with tf.name_scope('conv5_3') as scope:
    kernel = tf.Variable(tf.truncated_normal([3, 3, 512, 512], dtype=tf.float32,
                                             stddev=1e-1), name='weights')
    conv = tf.nn.conv2d(conv5_2, kernel, [1, 1, 1, 1], padding='SAME')
    biases = tf.Variable(tf.constant(0.0, shape=[512], dtype=tf.float32),
                         trainable=True, name='biases')
    out = tf.nn.bias_add(conv, biases)
    conv5_3 = tf.nn.relu(out, name=scope)
    en_parameters += [kernel, biases] #20

# pool5
pool5 = tf.nn.max_pool(conv5_3, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME', name='pool4') #10

training = True

# deconv6
with tf.variable_scope('deconv6') as scope:
    outputs = tf.layers.conv2d_transpose(pool5, 512, [3, 3], strides=(2, 2), padding='SAME',
                                         kernel_initializer=tf.contrib.layers.xavier_initializer())  #20
    deconv6 = tf.nn.relu(tf.layers.batch_normalization(outputs, training=training))
    concat6 = tf.concat([conv5_3 ,deconv6], axis=3)
    deconv6_1 = tf.layers.conv2d(concat6, 512, [3 , 3],strides = (1, 1), padding = 'SAME', activation=tf.nn.relu)
    deconv6_2 = tf.layers.conv2d(deconv6_1, 512, [3, 3], strides=(1, 1), padding='SAME', activation=tf.nn.relu)

# deconv5
with tf.variable_scope('deconv5') as scope:
    outputs = tf.layers.conv2d_transpose(deconv6_2, 512, [3, 3], strides=(2, 2), padding='SAME',
                                         kernel_initializer=tf.contrib.layers.xavier_initializer())  #40
    deconv5 = tf.nn.relu(tf.layers.batch_normalization(outputs, training=training))
    concat5 = tf.concat([conv4_3, deconv5], axis=3)

    deconv5_1 = tf.layers.conv2d(concat5, 512, [3, 3], strides=(1, 1), padding='SAME', activation=tf.nn.relu)
    deconv5_2 = tf.layers.conv2d(deconv5_1, 512, [3, 3], strides=(1, 1), padding='SAME', activation=tf.nn.relu)

# deconv4
with tf.variable_scope('deconv4') as scope:
    outputs = tf.layers.conv2d_transpose(deconv5_2, 256, [3, 3], strides=(2, 2), padding='SAME',
                                         kernel_initializer=tf.contrib.layers.xavier_initializer()) #80
    deconv4 = tf.nn.relu(tf.layers.batch_normalization(outputs, training=training))

    concat4 = tf.concat([conv3_3, deconv4], axis= 3)
    deconv4_1 = tf.layers.conv2d(concat4, 256, [3, 3], strides=(1, 1), padding='SAME', activation=tf.nn.relu)
    deconv4_2 = tf.layers.conv2d(deconv4_1, 256, [3, 3], strides=(1, 1), padding='SAME', activation=tf.nn.relu)

# deconv3
with tf.variable_scope('deconv3') as scope:
    outputs = tf.layers.conv2d_transpose(deconv4_2, 128, [3, 3], strides=(2, 2), padding='SAME',
                                         kernel_initializer=tf.contrib.layers.xavier_initializer())#160
    deconv3 = tf.nn.relu(tf.layers.batch_normalization(outputs, training=training))

    concat3 = tf.concat([conv2_2, deconv3], axis=3)
    deconv3_1 = tf.layers.conv2d(concat3, 128, [3, 3], strides=(1, 1), padding='SAME', activation=tf.nn.relu)
    deconv3_2 = tf.layers.conv2d(deconv3_1, 128, [3, 3], strides=(1, 1), padding='SAME', activation=tf.nn.relu)

# deconv2
with tf.variable_scope('deconv2') as scope:
    outputs = tf.layers.conv2d_transpose(deconv3_2, 64, [5, 5], strides=(2, 2), padding='SAME',
                                         kernel_initializer=tf.contrib.layers.xavier_initializer())  # 320
    deconv2 = tf.nn.relu(tf.layers.batch_normalization(outputs, training=training))
    concat2 = tf.concat([conv1_2, deconv2], axis=3)
    deconv2_1 = tf.layers.conv2d(concat2, 64, [3, 3], strides=(1, 1), padding='SAME', activation=tf.nn.relu)
    deconv2_2 = tf.layers.conv2d(deconv2_1, 64, [3, 3], strides=(1, 1), padding='SAME', activation=tf.nn.relu)

# deconv1
with tf.variable_scope('deconv1') as scope:
    outputs = tf.layers.conv2d_transpose(deconv2_2, 32, [5, 5], strides=(1, 1), padding='SAME',
                                         kernel_initializer=tf.contrib.layers.xavier_initializer())
    deconv1 = tf.nn.relu(tf.layers.batch_normalization(outputs, training=training))

# pred_alpha_matte
with tf.variable_scope('pred_mask') as scope:
    outputs = tf.layers.conv2d_transpose(deconv1, 2, [5, 5], strides=(1, 1), padding='SAME',
                                         kernel_initializer=tf.contrib.layers.xavier_initializer())
    mask_pred = tf.nn.sigmoid(outputs, name='mask')


with tf.variable_scope('loss') as scope:
    losses = tf.sqrt(tf.square(mask - mask_pred[:, :, :, 0]) + 1e-10)

    tf.summary.scalar('alpha_loss', tf.reduce_sum(losses))

    total_loss = tf.reduce_sum(losses) / train_batch_size

    tf.summary.scalar('total_loss', total_loss)

    global_step = tf.Variable(0, trainable=False)
    train_op = tf.train.AdamOptimizer(learning_rate=1e-5).minimize(total_loss, global_step=global_step)

    saver = tf.train.Saver(tf.trainable_variables(), max_to_keep=1)

    coord = tf.train.Coordinator()
    summary_op = tf.summary.merge_all()
    summary_writer = tf.summary.FileWriter(log_dir, tf.get_default_graph())

mask_output = tf.identity(mask_pred, name = 'mask_output')


sess = tf.Session()
sess.run(tf.global_variables_initializer())
tf.train.start_queue_runners(coord=coord,sess=sess)

if pretrained_model:
    weights = np.load(model_path)
    keys = sorted(weights.keys())
    for i, k in enumerate(keys):
        if i == 26:
            break
        sess.run(en_parameters[i].assign(weights[k]))
    print('finish loading vgg16 model')

else:
    print('Restoring pretrained model...')
    saver.restore(sess,'model/model.ckpt')
print('Finish restoring model')
sess.graph.finalize()

sess.run()
# TRAINING ..............................
path_image = 'image/Unet'
path_mask = 'image/Unet/Mask/Image/'

dirName, subdirList, fileList = list(os.walk('/'.join([path_image, 'Image'])))[0]

train_data = [os.path.join(dirName, i) for i in fileList]
# TRAINING
# for epoch in range(max_epochs):
#     # train
#     idx = 0
#     for current_batch_index in range(0, len(train_data), train_batch_size):
#         current_batchs = processing(train_data[current_batch_index: current_batch_index + train_batch_size], (320, 320))
#         lst_seg_ao = []
#         print(current_batch_index)
#         for i in train_data[current_batch_index:current_batch_index +train_batch_size]:
#             temp1 = '/'.join([path_mask, i.split('/')[-1]]).split('.')
#             temp1 = temp1[0]
#             lst_seg_ao.append('{}.png'.format(temp1))
#             print(lst_seg_ao)
#         current_labels_ao = processing(lst_seg_ao, (320, 320), True)
#
#         print(mask.shape)
#         print(current_labels_ao.shape)
#
#         sess_results = sess.run([train_op, total_loss], feed_dict={image_input: current_batchs, mask: current_labels_ao,})
#         print(' Epoch: ', epoch, " Loss:  %.16f" % sess_results[1])
#         if idx % 5000 == 0:
#             saver.save(sess, "model1/model.ckpt")
#         idx +=1

# EXPORT .......................

output_graph_def = tf.graph_util.convert_variables_to_constants(
            sess, # The session is used to retrieve the weights
            tf.get_default_graph().as_graph_def(), # The graph_def is used to retrieve the nodes
            ['input_tensor', 'mask_output']# The output node names are used to select the usefull nodes
        )

output_graph = "model/frozen_segment_670.pb"
with tf.gfile.GFile(output_graph, "wb") as f:
    f.write(output_graph_def.SerializeToString())
    print("%d ops in the final graph." % len(output_graph_def.node))

mask = sess.run()