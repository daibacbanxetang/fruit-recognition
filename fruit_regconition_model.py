import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import Input, Activation, Dense, Conv2D, MaxPooling2D, ZeroPadding2D, Flatten
from keras.optimizers import Adam
from keras.layers.normalization import BatchNormalization
from keras.utils.np_utils import to_categorical
from keras.callbacks import TensorBoard
from sklearn.model_selection import train_test_split

X_path = 'feature.npy'
X_path_origin = 'image_origin.npy'
y_path = 'label.npy'

X_asli = np.load(X_path_origin)
X = np.load(X_path)
y = np.load(y_path)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20,shuffle=False)