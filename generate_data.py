import cv2
import numpy as np
import random
from os import listdir
import numpy as np
from PIL import Image

height = width= 640

def loadimage(arr, n, name_of_fruit):
    label = []

def check_fill_image(array):
    for i in range(1, 5000):
        x = random.randrange(0, width - 100, 1)
        y = random.randrange(0, height - 100, 1)

        if len(array) == 0:
            return x, y
        else:
            test = 0
            for j in array:
                if (x + 100 < j[0] or x - 100 > j[0]) and (y + 100 < j[1] or y - 100 > j[1]):
                    test += 1

            if test == len(array):
                return x, y

    return None, None

def generate_image():
    apple = random.randrange(1, 493, 1)
    banana = random.randrange(1, 491, 1)
    orange = random.randrange(1, 480, 1)
    salak = random.randrange(1, 491, 1)
    strawberry = random.randrange(1, 739, 1)
    tomato = random.randrange(1, 480, 1)

    shape = 100
    u = 0

    fruit = ['Apple', 'Orange', 'Banana', 'Salak', 'Tomato', 'Strawberry']
    # for i in fruit:
    #     a = 0
    #     for j in listdir('data/train/' + i):
    #         a += 1
    #         u += 1
    #         image = cv2.imread('data/train/' + i + '/' + j, cv2.IMREAD_UNCHANGED)
    #         cv2.imwrite('image/Train/' + i + '/' + str(a) + '.jpg', image)
    #         print(u)

    apple_image = cv2.imread('image/Train/Apple/' + str(apple) + '.jpg', cv2.IMREAD_UNCHANGED)
    banana_image = cv2.imread('image/Train/Banana/' + str(banana) + '.jpg', cv2.IMREAD_UNCHANGED)
    orange_image = cv2.imread('image/Train/Orange/' + str(orange) + '.jpg', cv2.IMREAD_UNCHANGED)
    salak_image = cv2.imread('image/Train/Salak/' + str(salak) + '.jpg', cv2.IMREAD_UNCHANGED)
    strawberry_image = cv2.imread('image/Train/Strawberry/' + str(strawberry) + '.jpg', cv2.IMREAD_UNCHANGED)
    tomato_image = cv2.imread('image/Train/Tomato/' + str(tomato) + '.jpg', cv2.IMREAD_UNCHANGED)

    image = np.zeros((height,width,3), np.uint8)
    image[:] = (255, 255, 255)

    array = []

    x_apple, y_apple = check_fill_image(array)
    if x_apple != None:
        array.append((x_apple, y_apple))

    x_banana, y_banana = check_fill_image(array)
    if x_banana != None:
        array.append((x_banana, y_banana))

    x_orange, y_orange = check_fill_image(array)
    if x_orange != None:
        array.append((x_orange, y_orange))

    x_salak, y_salak = check_fill_image(array)
    if x_salak != None:
        array.append((x_salak, y_salak))

    x_strawberry, y_strawberry = check_fill_image(array)
    if x_strawberry != None:
        array.append((x_strawberry, y_strawberry))

    x_tomato, y_tomato = check_fill_image(array)
    if x_tomato != None:
        array.append((x_tomato, y_tomato))

    if x_apple != None:
        image[x_apple:x_apple+shape, y_apple:y_apple+shape] = apple_image
    if x_banana != None:
        image[x_banana:x_banana + shape, y_banana:y_banana + shape] = banana_image
    if x_orange != None:
        image[x_orange:x_orange + shape, y_orange:y_orange + shape] = orange_image
    if x_salak != None:
        image[x_salak:x_salak + shape, y_salak:y_salak + shape] = salak_image
    if x_strawberry != None:
        image[x_strawberry:x_strawberry + shape, y_strawberry:y_strawberry + shape] = strawberry_image
    if x_tomato != None:
        image[x_tomato:x_tomato + shape, y_tomato:y_tomato + shape] = tomato_image

    return image

def generate_mask(image):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    ret, thresh = cv2.threshold(gray, 240, 255, cv2.THRESH_BINARY)

    image[thresh == 255] = 0
    image[thresh != 255] = 255

    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))
    erosion = cv2.erode(image, kernel, iterations=1)

    return erosion

def jpg_to_png(image):
    pass

if __name__ == '__main__':
    # for i in range(3000, 3001):
    #     image = generate_image()
    #     cv2.imwrite('image/Train/Segmentation/' + str(i) + '.jpg', image)

    for i in range(1, 3001, 1):
        image = cv2.imread('image/Train/Segmentation/' + str(i) + '.jpg')
        mask = generate_mask(image)
        print(i)
        cv2.imwrite('image/Unet/Mask/' + str(i) + '.png', mask)







