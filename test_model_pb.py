import os, argparse
import numpy as np
import tensorflow as tf

# The original freeze_graph function
# from tensorflow.python.tools.freeze_graph import freeze_graph

dir = os.path.dirname(os.path.realpath(__file__))


def freeze_graph(model_dir, output_node_names):
    """Extract the sub graph defined by the output nodes and convert
    all its variables into constant
    Args:
        model_dir: the root folder containing the checkpoint state file
        output_node_names: a string, containing all the output node's names,
                            comma separated
    """
    if not tf.gfile.Exists(model_dir):
        raise AssertionError(
            "Export directory doesn't exists. Please specify an export "
            "directory: %s" % model_dir)

    if not output_node_names:
        print("You need to supply the name of a node to --output_node_names.")
        return -1

    # We retrieve our checkpoint fullpath
    checkpoint = tf.train.get_checkpoint_state(model_dir)
    input_checkpoint = checkpoint.model_checkpoint_path

    # We precise the file fullname of our freezed graph
    absolute_model_dir = "/".join(input_checkpoint.split('/')[:-1])
    output_graph = absolute_model_dir + "/frozen_model1.pb"

    # We clear devices to allow TensorFlow to control on which device it will load operations
    clear_devices = True

    # We start a session using a temporary fresh Graph
    with tf.Session(graph=tf.Graph()) as sess:
        # We import the meta graph in the current default Graph
        saver = tf.train.import_meta_graph(input_checkpoint + '.meta', clear_devices=clear_devices)

        # We restore the weights
        saver.restore(sess, input_checkpoint)

        # We use a built-in TF helper to export variables to constants
        output_graph_def = tf.graph_util.convert_variables_to_constants(
            sess,  # The session is used to retrieve the weights
            tf.get_default_graph().as_graph_def(),  # The graph_def is used to retrieve the nodes
            output_node_names.split(",")  # The output node names are used to select the usefull nodes
        )

        # Finally we serialize and dump the output graph to the filesystem
        with tf.gfile.GFile(output_graph, "wb") as f:
            f.write(output_graph_def.SerializeToString())
        print("%d ops in the final graph." % len(output_graph_def.node))

    return output_graph_def


def load_graph(frozen_graph_filename):
    # We load the protobuf file from the disk and parse it to retrieve the
    # unserialized graph_def
    with tf.gfile.GFile(frozen_graph_filename, "rb") as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())

    # Then, we import the graph_def into a new Graph and returns it
    with tf.Graph().as_default() as graph:
        # The name var will prefix every op/nodes in your graph
        # Since we load everything in a new graph, this is not needed
        tf.import_graph_def(graph_def, name="")
    return graph


def using(input, h, w):
    graph = load_graph(r'E:\Intern\Python\Fruit-Recognition\model\frozen_segment_670.pb')
    # for op in graph.get_operations():
    #     print(op.name)

    x = graph.get_tensor_by_name('input_tensor:0')
    y = graph.get_tensor_by_name('mask_output:0')

    # yy = tf.nn.sigmoid(y)
    with tf.Session(graph=graph) as sess:
        # Note: we don't nee to initialize/restore anything
        # There is no Variables in this graph, only hardcoded constants
        y_out = sess.run(y, feed_dict={
            x: input  # < 45
        })
    #
    mask = y_out[:, :, :, 0]*255
    cv2.imshow('im',cv2.resize(np.squeeze(mask), (h, w)).astype(np.uint8))
    cv2.waitKey()
    cv2.destroyAllWindows()

if __name__ == '__main__':
    # parser = argparse.ArgumentParser()
    # parser.add_argument("--model_dir", type=str, default="", help="Model folder to export")
    # parser.add_argument("--output_node_names", type=str, default="",
    #                     help="The name of the output nodes, comma separated.")
    # args = parser.parse_args()
    import cv2
    # im = cv2.imread("/media/duongpd/Data/data/train/image/063311.jpg")
    im = cv2.imread(r"E:\Intern\Python\Fruit-Recognition\Untitled.png")
    h, w, c = im.shape
    im = cv2.resize(im, (320, 320))
    im = im / 255.
    im = np.expand_dims(im, axis=0)

    using(im, h=h, w=w)