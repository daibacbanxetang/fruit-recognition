import numpy as np
import matplotlib.pyplot as plt
import os
import cv2
from skimage.transform import rescale, resize, downscale_local_mean
from keras.utils.np_utils import to_categorical
import random

label = ["Apple", "Banana", "Blueberry", "Cherry", "Chestnut", "Kaki", "Kiwi", "Lemon", "Limes", "Lychee", "Mango", "Orange", "Papaya", "Pear", "Pepino", "Physalis", "Pineapple", "Plum", "Pomegranate", "Raspberry", "Salak", "Strawberry", "Tomato"]
path_data = 'image/Train'
training_data = []

for i in label:
    path = os.path.join(path_data, i)
    index = label.index(i)
    for image in os.listdir(path):
        image_arr = cv2.imread(os.path.join(path, image), cv2.IMREAD_GRAYSCALE)
        resize = cv2.resize(image_arr, (224, 224))
        a = image_arr
        # training_data.append([resize, index])
cv2.imshow()
random.shuffle(training_data)

X = []
Y = []

for feature, label in training_data:
    X.append(feature)
    Y.append(label)

np.save('image_origin.npy', X)

X = np.array(X).reshape(-1, 224, 224, 1)
Y_binary = to_categorical(Y)

np.save("feature.npy",X)
np.save("label.npy",Y_binary)

